var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var path = require('path');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var nodemailer = require('nodemailer');
var sliderCaptcha = require('@slider-captcha/core');
var AccessControl = require('express-ip-access-control');
require('dotenv').config();
global.moment = require('moment');

process.env.TZ = 'Asia/Jakarta'
global.app = express();
global.appDir = path.resolve(__dirname);
global.admin = require("firebase-admin");

var optionsAccessControl = {
	mode: 'allow',
	allows: ['127.0.0.1', '::1'],
	forceConnectionAddress: false,
	log: function(clientIp, access) {
		console.log(clientIp + (access ? ' accessed.' : ' denied.'));
	},

	statusCode: 401,
	redirectTo: '',
	message: 'Unauthorized'
};

app.use(AccessControl(optionsAccessControl));

// cors
var cors = require('cors');
app.use(
  cors({
    origin: [process.env.APP_CLIENT, process.env.APP_DASHBOARD],
    credentials: true,
  })
);

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Headers', true);
  res.header('Access-Control-Allow-Credentials', "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Origin");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  next();
});

app.use(logger('dev'));
app.use(cookieParser());
app.use(express.json({limit: '20mb'}));
app.use(express.urlencoded({limit: '20mb', extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

var captcha_session = [];

app.get('/captcha/create/:id', function (req, res) {
  sliderCaptcha.create()
    .then(function ({data, solution}) {
      data.id = solution;
      captcha_session.push({
        id: req.params.id,
        solution: solution
      })
      res.status(200).send(data);
    });
});

app.post('/captcha/verify/:id', function (req, res) {
  var captcha = captcha_session.find((captcha) => captcha.id === req.params.id)
  sliderCaptcha.verify(captcha.solution, req.body)
    .then(function (verification) {
      res.status(200).send(verification);
    });
});

//encrypt
const Cryptr = require('cryptr');
const cryptr = new Cryptr('RAJ1411UMKM');
global.encrypt = (obj) => {
  const encryptedString = cryptr.encrypt(obj);
  return encryptedString
}

global.decrypt = (obj) => {
  const decryptedString = cryptr.decrypt(obj);
  return decryptedString
}

global.UUID = (obj) => {
  return obj+"-"+moment().format('YYMMDDHH-mmssSSSS')
}
//socket

//middleware authenticateJWT
global.auth = require(appDir+'/app/middleware/Auth');

// model & controllers
global.model = require(appDir+'/app/models/index');
var controllers_router = require(appDir+'/config/routes/index');
controllers_router();

let transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    secure: false,
    requireTLS: true,
    port: process.env.SMTP_PORT,
    auth: {
      user: process.env.SMTP_MAIL,
      pass: process.env.SMTP_PASS // naturally, replace both with your real credentials or an application-specific password
    }
});


global.SendMail = async(info) =>{
  transporter.sendMail({
      from: 'Appsinstant <no-reply@appsinstant.com>',
      to: info.to,
      subject: 'Appsinstant | '+info.subject,
      html: info.html,
  });
}

// upload files
global.uploadFile = async (file,type,id) => {
  try {
    if(!file){
      new Error('file not found');
    }

    const imgdata = file;
    const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
    const extension = imgdata.match(/[^:/]\w+(?=;|,)/)[0];
    const path = appDir+'/public/files/'+type+"-"+id+"."+extension
    await fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
    return path.replace(appDir+'/public/', '/');
  } catch (e) {
    console.error(e)
    return false
  }
}

// upload images
global.uploadImage = async (image,type,id) => {
  try {
    if(!image){
      new Error('image not found');
    }

    const imgdata = image;
    const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
    const extension = imgdata.match(/[^:/]\w+(?=;|,)/)[0];
    const path = appDir+'/public/images/'+type+"-"+id+"."+extension
    await fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
    return path.replace(appDir+'/public/', '/');
  } catch (e) {
    console.error(e)
    return false
  }
}

// delete image
global.deleteImage = async (image) => {
  try {
    await fs.unlink(appDir+'/public'+image, (err) => {
      if (err) throw err;
      console.log(`${image} was deleted`);
    });
    return true
  } catch (e) {
    log.error(e)
    return false
  }
}

global.LibUsers = require(appDir+'/app/lib/LibUsers');
global.LibNotification = require(appDir+'/app/lib/Notification');


//////////////////////// cronjob ////////////////////////////
var cron = require(appDir+'/cron/index');
cron();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404).json({
    'status': 'error',
    'messages': 'Not Found',
    'result': ''
  })
});

// error handler
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.status(err.status || 500).json({
    'status': 'error',
    'messages': res.locals.message,
    'result': res.locals.error
  })
});

module.exports = app;
