var controllers_router = function(){
  const route = '/api/v1';
  const route_path = appDir + '/app/controllers/api/v1';

  var auth = require(route_path+'/Auth/AuthControllers');
  app.use(route+'/oauth', auth);

  var profile = require(route_path+'/Profile/ProfileControllers');
  app.use(route+'/profile', profile);

}

module.exports = controllers_router;