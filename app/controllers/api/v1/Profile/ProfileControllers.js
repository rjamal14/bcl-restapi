const express = require('express');
const router = express.Router();

// update profile
router.patch('/:id', auth(), async(req, res, next) => {
    var data = req.body

    const profile = await model.profile.findOne({where:{ user_id: req.params.id}})
    var date = new Date();
    var ran = Math.floor(Math.random() * date.getTime())
    if(profile){
        const photo = await uploadImage(req.body.photo, 'photo', ran+'-'+await req.params.id);
        data.photo = (req.body.photo) ? await photo : await profile.photo;

        if(photo){
            deleteImage(await profile.photo);
        }

        await profile.update(await data, { returning: true, plain: true }).then((profile)=>{
            res.status(200).json({
                status: 'success',
                message: 'Successfully',
                result: profile,
            });
        }).catch((error)=>{
            return res.status(200).json({
                code: 40,
                status: "error",
                message: error.name,
                result:error,
            });
        })
    }

});


module.exports = router;