'use strict';
const { Model } = require('sequelize');
const sequelizePaginate = require('sequelize-paginate');

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    static associate(models) {
      models.user.hasOne(models.profile, { foreignKey: 'user_id', as: 'profile' })
    }
  };

  user.init({
    username: DataTypes.STRING,
    password: DataTypes.TEXT,
    role: DataTypes.ENUM('superadmin', 'marketing', 'legal', 'finance', 'operational')
  }, {
    sequelize,
    modelName: 'user',
    tableName: 'users',
    paranoid: true,
  });

  sequelizePaginate.paginate(user);

  return user;
};